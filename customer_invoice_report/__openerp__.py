# -*- coding: utf-8 -*-
{
    'name': 'Customer Invoice Report',
    'version': '1.0',
    'depends': ['account_accountant', 'sale'],
    'author': 'Port Cities Indonesia',
    'description': """

        * v 1.0
        - This is custom module to learn simple logic python and how to create xlsx report within odoo
        - Author : aziz@portcities.net

    """,
    'summary': 'Customer Invoice Report',
    'website': 'http://www.portcitiesindonesia.com',
    'category': 'Report',
    'sequence': 1,
    'data': [
        'views/customer_invoice_report_view.xml',
    ],
    'auto_install': False,
    'installable': True,
    'application': False,
}
