odoo.define('sample.javascript', function (require) {'use strict';

    var core = require('web.core');
    var Model = require('web.DataModel');
    var FormView = require('web.FormView');
    var ListView = require('web.ListView');
    var KanbanView = require('web_kanban.KanbanView');

    var Class = core.Class;
    var _t = core._t;
    var _lt = core._lt;
    var QWeb = core.qweb;

    // Task 14 //

    var custom_button_form = FormView.include({
        render_buttons: function($node) {
            this._super($node);
            if (this.model == 'product.template') {
                if (this.$buttons) {
                    this.$buttons.find('.oe_form_button_create')[0].innerHTML = 'New Product';
                }
            }
        }
    });

    var custom_button_kanban = KanbanView.include({
        render_buttons: function($node) {
            this._super($node);
            if (this.model == 'product.template') {
                if (this.$buttons) {
                    this.$buttons.find('.o-kanban-button-new')[0].innerHTML = 'New Product';
                }
            }
        }
    });

    var custom_button_list = ListView.include({
        render_buttons: function($node) {
            this._super($node);
            if (this.model == 'product.template') {
                if (this.$buttons) {
                    this.$buttons.find('.o_list_button_add')[0].innerHTML = 'New Product';
                }
            }
        }
    });

    // Task (Draft) //

    var product_template = new Model('product.template');

    var append_popover = ListView.include({
        do_show: function () {
            this._super();
            if (this.model == 'product.template') {
                this.$el.find('tr[data-id]').on('mouseover', product_mouseover).on('mouseout', product_mouseout);
            }
        }
    });

    function product_mouseover() {
        var data_id = $(this).attr('data-id');

        product_template.call('product_image', [data_id]).then(function(image) {
            var mouseX = 0;
            var mouseY = 0;

            $('#popup').remove();
            if (image) {
                $('.oe-view-manager-view-list').append(
                    '<div id="popup" class="image-popup"><img src="data:image/jpg;base64,' + image + '" /></div>'
                );
            } else {
                $('.oe-view-manager-view-list').append(
                    '<div id="popup" class="image-popup">No Image</div>'
                );
            }
            $(document).mousemove(function(e) {
                mouseX = e.pageX + 20;
                mouseY = e.pageY + 20;
                $('#popup').css({'top':mouseY,'left':mouseX});
            });
        });
    };

    function product_mouseout() {
        $('#popup').remove();
    };
});