# -*- coding: utf-8 -*-
{
    'name': 'Engineer Learning Task',
    'version': '1.0',
    'depends': ['base', 'purchase', 'sale', 'stock'],
    'author': 'Port Cities',
    'description': """Modules for Engineer Learning Task.""",
    'website': 'http://www.portcities.net',
    'category': 'Sample',
    'sequence': 1,
    'summary': 'Engineer Learning Task',
    'data': [
        'views/learning_task.xml',
        'views/learning_task_views.xml',
        'report/report.xml',
        'report/report_product_tag.xml'
    ],
    'auto_install': False,
    'installable': True,
    'application': False,
}
