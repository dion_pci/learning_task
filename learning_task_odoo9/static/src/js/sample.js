odoo.define('sample.javascript', function (require) {'use strict';

    var core = require('web.core');
    var Model = require('web.DataModel');
    var FormView = require('web.FormView');
    var ListView = require('web.ListView');
    var KanbanView = require('web_kanban.KanbanView');

    // Task 14 //

    var customButtonForm = FormView.include({
        render_buttons: function($node) {
            this._super($node);
            if (this.model == 'product.template') {
                if (this.$buttons) {
                    this.$buttons.find('.oe_form_button_create')[0].innerHTML = 'New Product';
                }
            }
        }
    });

    var customButtonKanban = KanbanView.include({
        render_buttons: function($node) {
            this._super($node);
            if (this.model == 'product.template') {
                if (this.$buttons) {
                    this.$buttons.find('.o-kanban-button-new')[0].innerHTML = 'New Product';
                }
            }
        }
    });

    var customButtonList = ListView.include({
        render_buttons: function($node) {
            this._super($node);
            if (this.model == 'product.template') {
                if (this.$buttons) {
                    this.$buttons.find('.o_list_button_add')[0].innerHTML = 'New Product';
                }
            }
        }
    });

    // Task (Draft) //

    var productTemplate = new Model('product.template');

    var appendPopover = ListView.include({
        do_show: function () {
            this._super();
            console.log("Test");
            if (this.model == 'product.template') {
                this.$el.find('tr[data-id]').on('mouseover', productMouseover).on('mouseout', productMouseout);
            }
        }
    });

    function productMouseover() {
        var data = $(this).attr('data-id');

        productTemplate.call('product_image', [data]).then(function(image) {
            var mouseX = 0;
            var mouseY = 0;

            $('#popup').remove();
            if (image) {
                $('.oe-view-manager-view-list').append(
                    '<div id="popup" class="image-popup"><img src="data:image/jpg;base64,' + image + '" /></div>'
                );
            } else {
                $('.oe-view-manager-view-list').append(
                    '<div id="popup" class="image-popup">No Image</div>'
                );
            }
            $(document).mousemove(function(e) {
                mouseX = e.pageX + 20;
                mouseY = e.pageY + 20;
                $('#popup').css({'top':mouseY,'left':mouseX});
            });
        });
    }

    function productMouseout() {
        $('#popup').remove();
    }

    return customButtonForm, customButtonKanban, customButtonList, appendPopover;
});
