# coding: utf-8

from openerp import api, exceptions, fields, models


# Task 1 #

class Task_1_to_4(models.Model):
    _inherit = 'purchase.order'

    supplier_phone = fields.Char(related='partner_id.phone', string='Phone')
    supplier_email = fields.Char(related='partner_id.email', string='Email')

# Task 2 #

    contact_person = fields.Many2one('res.users', string='Contact Person',
                                     default=lambda self: self.env.user)

# Task 3 #

    @api.one
    @api.depends('date_planned', 'date_order')
    def _compute_duration(self):
        if not self.date_planned:
            self.date_planned = fields.Date.today()
        dp = fields.Date.from_string(self.date_planned)
        do = fields.Date.from_string(self.date_order)
        if dp > do:
            self.duration = (dp - do).days
        else:
            self.duration = 0

    duration = fields.Float('Duration', compute='_compute_duration')

# Task 4 #

    @api.multi
    def action_view_related_products(self):
        term_domain = []
        for line in self.order_line:
            term_domain.append(line.id)
        domain = [('id', 'in', term_domain)]
        return {
            'name': ('Related Products'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree, form',
            'res_model': 'product.template',
            'target': 'current',
            'domain': domain
        }


# Task 5 #

class Task_5(models.Model):
    _inherit = 'stock.picking'

    confirm_date = fields.Datetime(string='Confirmed Date')
    confirm_user = fields.Many2one('res.users', string='Confirmed By')

    @api.multi
    @api.depends('confirm_date', 'confirm_user')
    def action_confirm(self):
        self.confirm_date = fields.Date.today()
        self.confirm_user = self.env.user
        return super(Task_5, self).action_confirm()


# Task 6 #

class Task_6(models.Model):
    _inherit = 'purchase.order'

    @api.model
    def create(self, values):
        if not self.partner_ref:
            values['partner_ref'] = 'No Supplier Reference'
        return super(Task_6, self).create(values)

    @api.multi
    def write(self, values):
        if not self.partner_ref:
            values['partner_ref'] = 'No Supplier Reference'
        return super(Task_6, self).write(values)


# Task 7 #

# class Task_7(models.Model):
#     _inherit = 'product.template'

#     @api.model
#     def create(self, values):
#         if not self.seller_ids:
#             res_partner_obj = self.env['res.partner'].search([
#                 ('name', '=', 'No Supplier')])
#             prod_supplier_obj = self.env['product.supplierinfo']
#             prod_supplier_obj.create({
#                 'name': res_partner_obj.id,
#                 'product_tmpl_id': self.id
#             })
#         return super(Task_7, self).create(values)

#     @api.multi
#     def write(self, values):
#         if not self.seller_ids:
#             res_partner_obj = self.env['res.partner'].search([
#                 ('name', '=', 'No Supplier')])
#             prod_supplier_obj = self.env['product.supplierinfo']
#             prod_supplier_obj.create({
#                 'name': res_partner_obj.id,
#                 'product_tmpl_id': self.id
#             })
#         return super(Task_7, self).write(values)


# Task 9 #

class Task_9(models.Model):
    _inherit = 'purchase.order'

    down_payment = fields.Float(string='Down Payment (DP)', store=True)

    @api.one
    @api.depends('amount_total', 'down_payment')
    def get_amount_total_dp(self):
        if not self.down_payment:
            self.down_payment = 0
        tp = self.amount_total
        dp = self.down_payment
        self.amount_total_dp = (tp - dp)

    amount_total_dp = fields.Float(string='Total', store=True,
                                   compute='get_amount_total_dp')


# Task 10 #

# class Task_10(models.Model):
#     _inherit = 'product.template'

#     @api.model
#     def create(self, values):
#         if not self.seller_ids:
#             raise exceptions.Warning(
#                 'You should fill in the supplier details, at least one.')
#         return super(Task_10, self).create(values)

#     @api.multi
#     def write(self, values):
#         if not self.seller_ids:
#             raise exceptions.Warning(
#                 'You should fill in the supplier details, at least one.')
#         return super(Task_10, self).write(values)


# Task 11 #

class Task_11(models.Model):

    _inherit = 'sale.order'

    @api.multi
    def action_cancel(self):
        status = self.env['purchase.order'].search([
            ('origin', '=', self.name)])
        for origin in status:
            origin.write({'state': 'cancel'})
        return super(Task_11, self).action_cancel()

    @api.multi
    def action_draft(self):
        status = self.env['purchase.order'].search([
            ('origin', '=', self.name)])
        for origin in status:
            origin.write({'state': 'draft'})
        self.write({'state': 'draft'})
        return super(Task_11, self).action_draft()


# Task Draft #

class task_draft(models.Model):
    _inherit = 'product.template'

    @api.model
    def product_image(self, product_id):
        product_image = self.search([('id', '=', product_id)])[0].image_medium
        return product_image
