odoo.define('user_menu.UserMenu', function (require) {
    "use strict";

    var core = require('web.core');
    var session = require('web.session');
    var Model = require('web.Model');
    var UserMenu = require('web.UserMenu');

    var _t = core._t;
    var QWeb = core.qweb;

    UserMenu.include({
        on_menu_change_password: function() {
            this.do_action({
                name: 'Change Password',
                type: 'ir.actions.client',
                tag: 'change_password',
                target: 'new',
            });
        },
        on_menu_reset_password: function() {
            new Model('res.users').call('reset_password', [[], session.username]);
        },
    });

});
