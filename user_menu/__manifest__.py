# -*- coding: utf-8 -*-
{
    'name': 'User Menu',
    'version': '10.01.0.0',
    'summary': 'Add "Change Password" option in user menu.',
    'depends': ['web'],
    'data': ['views/webclient_templates.xml'],
    'qweb': ['static/src/xml/base.xml'],
    'installable': True,
    'auto_install': False,
    'application': False,
}
