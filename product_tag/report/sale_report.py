# coding: utf-8
from odoo import fields, models


class SaleReport(models.Model):
    _inherit = "sale.report"

    product_tag = fields.Many2one('product.tag', 'Tags', readonly=True)

    def _select(self):
        return super(
            SaleReport, self)._select() + ", t.product_tag as product_tag"

    def _group_by(self):
        return super(SaleReport, self)._group_by() + ", t.product_tag"
