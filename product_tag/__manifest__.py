# -*- coding: utf-8 -*-
{
    'name': 'Product Tag',
    'version': '1.0',
    'depends': ['stock', 'sale'],
    'author': 'Port Cities',
    'description': """ """,
    'website': 'http://www.portcities.net',
    'category': 'Warehouse',
    'sequence': 5,
    'summary': 'Sale Order',
    'data': [
        'views/product_tag_views.xml',
        'views/sale_promo_views.xml',
        'report/sale_report.xml'
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}
