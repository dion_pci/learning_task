# coding: utf-8
from odoo import api, fields, models


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    promo_ids = fields.One2many('sale.promo', 'sale_id', string='Promotion')

    @api.multi
    def action_confirm(self):
        for order in self:
            order.state = 'sale'
            order.confirmation_date = fields.Datetime.now()
            if self.env.context.get('send_email'):
                self.force_quotation_send()
            order.order_line._action_procurement_create()
            order.promo_ids._action_procurement_create()
        if self.env['ir.values'].get_default(
                'sale.config.settings', 'auto_done_setting'):
            self.action_done()
        return True
