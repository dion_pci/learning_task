# coding: utf-8
from odoo import fields, models


class ProcurementOrder(models.Model):
    _inherit = 'procurement.order'

    promo_id = fields.Many2one('sale.promo', string='Promotion')
