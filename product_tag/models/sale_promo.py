# coding: utf-8
from datetime import datetime

from odoo import api, fields, models
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare


class SalePromo(models.Model):
    _description = 'Sale Promo'
    _name = 'sale.promo'
    _order = 'sequence'

    name = fields.Text(string='Description', required=True)
    qty = fields.Float(string='Quantity', default=1.0)
    sequence = fields.Integer(string='Sequence', default=0)

    sale_id = fields.Many2one(
        'sale.order', string='Order Reference', required=True,
        ondelete='cascade', index=True, copy=False)
    product_id = fields.Many2one(
        'product.product', string='Product',
        change_default=True, ondelete='restrict')
    product_uom = fields.Many2one(
        'product.uom', string='Unit of Measure', required=True)
    procurement_ids = fields.One2many(
        'procurement.order', 'promo_id', string='Procurement')

    state = fields.Selection([
        ('draft', 'Quotation'),
        ('sent', 'Quotation Sent'),
        ('sale', 'Sale Order'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
    ], related='sale_id.state', string='Order Status')

    @api.multi
    @api.onchange('product_id')
    def product_id_change(self):
        if not self.product_id:
            return None

        uom_id = self.product_id.uom_id
        if not self.product_uom or (uom_id.id != self.product_uom.id):
            self.product_uom = uom_id

        name = self.product_id.name_get()[0][1]
        if self.product_id.description_sale:
            name += '\n' + self.product_id.description_sale

        self.update({
            'name': name,
            'product_uom': uom_id,
        })

    @api.multi
    def _prepare_promo_procurement(self, group_id=False):
        self.ensure_one()

        default_time = datetime.strptime(
            self.sale_id.date_order, DEFAULT_SERVER_DATETIME_FORMAT).strftime(
            DEFAULT_SERVER_DATETIME_FORMAT)
        location = self.sale_id.partner_shipping_id.property_stock_customer.id
        warehouse = self.sale_id.warehouse_id and self.sale_id.warehouse_id.id

        return {
            'name': self.name,
            'origin': self.sale_id.name,
            'date_planned': default_time,
            'product_id': self.product_id.id,
            'product_qty': self.qty,
            'product_uom': self.product_uom.id,
            'company_id': self.sale_id.company_id.id,
            'location_id': location,
            'warehouse_id': warehouse,
            'partner_dest_id': self.sale_id.partner_shipping_id.id,
            'group_id': group_id,
            'promo_id': self.id
        }

    @api.multi
    def _action_procurement_create(self):
        precision = self.env[
            'decimal.precision'].precision_get('Product Unit of Measure')
        new_procs = self.env['procurement.order']  # Empty recordset
        for line in self:
            if line.state != 'sale' or not line.product_id._need_procurement():
                continue
            qty = 0.0
            for proc in line.procurement_ids:
                qty += proc.product_qty
            if float_compare(qty, line.qty, precision_digits=precision) >= 0:
                continue

            if not line.sale_id.procurement_group_id:
                vals = line.sale_id._prepare_procurement_group()
                line.sale_id.procurement_group_id = self.env[
                    "procurement.group"].create(vals)

            vals = line._prepare_promo_procurement(
                group_id=line.sale_id.procurement_group_id.id)
            vals['product_qty'] = line.qty - qty
            new_proc = self.env[
                "procurement.order"].with_context(
                    procurement_autorun_defer=True).create(vals)
            new_proc.message_post_with_view(
                'mail.message_origin_link',
                values={'self': new_proc, 'origin': line.sale_id},
                subtype_id=self.env.ref('mail.mt_note').id)
            new_procs += new_proc
        new_procs.run()
        return new_procs
