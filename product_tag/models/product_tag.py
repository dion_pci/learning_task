# coding: utf-8
from odoo import api, fields, models


class PartnerCategory(models.Model):
    _inherit = 'res.partner'

    product_category = fields.Many2one(
        'product.category', string="Product Categories")

    product_tag = fields.Many2one(
        'product.tag', string='Product Tag',
        domain="[('tag_type', '=', 'brand')]")

    # def _get_category(self):
    #     return self.env['product.category'].search(
    #         [('type', 'in', ['income', 'value', 'excise'])]).ids


class SaleOrderCategory(models.Model):
    _inherit = 'sale.order'

    related_category = fields.Many2one(
        'product.category', string='Related Product Categories',
        related='partner_id.product_category')

    related_tag = fields.Many2one(
        'product.tag', string='Related Product Tag',
        related='partner_id.product_tag',
        domain="[('selection', '=', 'brand')]")

    # @api.multi
    # @api.onchange('partner_id')
    # def _check_child_ids(self):
    #     print('\n')
    #     print('Category ID: %s' % self.related_category)
    #     if self.related_category.child_id:
    #         print('Child ID: %s' % self.related_category.child_id)
    #     print('\n')

    @api.multi
    @api.onchange('related_category', 'related_tag')
    def _adjust_order_line(self):
        temp_order_line = self.order_line.ids

        for line in self.order_line:
            if line.product_id.categ_id != self.related_category and \
                    line.product_id.categ_id not in \
                    self.related_category.child_id and \
                    line.product_id.product_tags not in \
                    self.related_tag:
                temp_order_line.remove(line.id)

        self.update({
            'order_line': temp_order_line
        })


# Product Tag
class ProductTag(models.Model):
    _description = 'Product Tag'
    _name = 'product.tag'

    name = fields.Char(string='Name', required=True)

    tag_type = fields.Selection(
        [('general', 'General'), ('brand', 'Brand')],
        string='Type', required=True, default='general')

    customer_ids = fields.One2many('res.partner', 'product_tag', 'Customer')

    product_ids = fields.Many2many(
        'product.template', string='Products')

    _sql_constraints = [
        ('tag_name_unique', 'unique(name)', 'The tag name must be unique!'),
    ]


class ProductTemplateTags(models.Model):
    _inherit = 'product.template'

    product_tags = fields.Many2many('product.tag', string='Tags', store=True)
