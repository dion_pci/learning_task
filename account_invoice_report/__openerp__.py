# -*- coding: utf-8 -*-
{
    'name': 'Account Invoice Report',
    'version': '1.0',
    'depends': ['stock', 'sale', 'account'],
    'author': 'Port Cities Indonesia',
    'description': """ """,
    'website': 'http://www.portcitiesindonesia.com',
    'category': 'Report',
    'data': [
        'reports/report.xml',
        'reports/account_invoice_report.xml',
    ],
    'auto_install': False,
    'installable': True,
}
