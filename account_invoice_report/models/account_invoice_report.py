# -*- coding: utf-8 -*-
from openerp import api, models


class AccountInvoiceReport(models.AbstractModel):
    _name = 'report.account_invoice_report.account_invoice_report'

    def get_line_discont(self):
        return

    @api.model
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name(
            'account_invoice_report.account_invoice_report')
        docargs = {
            'doc_ids': docids,
            'doc_model': report.model,
            'docs': self.env[report.model].browse(docids),
        }
        print('\n')
        print('Model: %s' % report.model)
        print(docargs['docs'])
        print('\n')
        return report_obj.render(
            'account_invoice_report.account_invoice_report', docargs)
