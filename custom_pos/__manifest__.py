# -*- coding: utf-8 -*-
{
    'name': 'Custom POS',
    'version': '1.0',
    'depends': ['point_of_sale'],
    'author': 'Port Cities',
    'description': ''' ''',
    'data': [
        'views/point_of_sale.xml',
    ],
    'qweb': [
        'static/src/xml/pos.xml'
    ],
}
