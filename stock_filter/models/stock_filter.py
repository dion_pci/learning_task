# -*- coding: utf-8 -*-
from odoo import fields, models


class StockPickingFilter(models.Model):
    _inherit = "stock.picking.type"

    picking_ready = fields.Boolean(
        string='Ready', compute='_compute_picking_status',
        search='_search_picking_ready')

    def _compute_picking_status(self):
        domain = [('state', '=', 'assigned')]
        move_ids = self.env['stock.picking'].search(domain)

        picking_type = self.env['stock.picking.type'].search([])
        picking_ids = []

        for move in move_ids:
            if move.picking_type_id not in picking_ids:
                picking_ids.append(move.picking_type_id)

        for picking in picking_type:
            if picking in picking_ids:
                picking.picking_ready = True
            else:
                picking.picking_ready = False

    def _search_picking_ready(self, operator, value):
        self._compute_picking_status()

        picking_type = self.search([])
        search_result = []

        for picking in picking_type:
            if picking.picking_ready is True:
                search_result.append(picking.id)

        # return search_result
        return [('id', 'in', search_result)]
