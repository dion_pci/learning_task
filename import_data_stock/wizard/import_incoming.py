# -*- coding: utf-8 -*-
import base64
from datetime import datetime
import xlrd
from odoo import _, fields, models, api
from odoo.exceptions import UserError, Warning


class ImportIncoming(models.TransientModel):
    _name = 'import.incoming'
    _description = 'Import Incoming Stock'

    file_data = fields.Binary(string='File XLS', required=True)
    file_name = fields.Char(string='File XLS')

    @api.onchange('file_name')
    def change_filename(self):
        if not self.file_name:
            self.file_data = False

    def action_import(self):
        context = self._context
        active_ids = context.get('active_ids', [])
        try:
            book = xlrd.open_workbook(file_contents=base64.decodestring(self.file_data))
            sheet = book.sheet_by_index(0)

            # temporary data and picking(per docs)
            all_pickings = []
            all_data = []
            # read and loops xlsx data
            for row in range(sheet.nrows):
                if row == 0:
                    continue
                sku_code = sheet.cell(row, 0).value
                sku_desc = sheet.cell(row, 1).value
                qty = sheet.cell(row, 2).value
                return_qty = sheet.cell(row, 3).value
                customer = sheet.cell(row, 4).value
                id_input = sheet.cell(row, 5).value
                doc = sheet.cell(row, 6).value
                int_ref = sheet.cell(row, 7).value
                type_int_ref = type(int_ref)
                int_ref = int(int_ref) if type_int_ref == float else int_ref
                
                all_pickings.append(str(doc))
                all_data.append({
                            'sku_code': sku_code if sku_code else None,
                            'sku_desc': sku_desc if sku_desc else None,
                            'qty': qty if qty else 0.0,
                            'return_qty': return_qty,
                            'customer': customer if customer else None,
                            'id_input': int(id_input) if id_input else None,
                            'doc': str(doc),
							'internal_ref': str(int_ref) if int_ref else None
                        })

            picking_obj = self.env['stock.picking']
            move_obj = self.env['stock.move']
            product_obj = self.env['product.product']
            partner_obj = self.env['res.partner']
            pickings = [picking for picking in set(all_pickings)]
            for picking in pickings:
                moves = []
                internal_notes = None
                source_document = None
                partner_id = None
                for data in all_data:
                    if picking == data.get('doc'):
                        internal_notes = data.get('customer')
                        source_document = data.get('doc')
                        if internal_notes:
                            partner_id = partner_obj.search([('comment', '=', internal_notes), ('customer', '=', True)])
                            partner_id = partner_id.id
                        else:
                            partner_id = None

                # create picking based on doc
                pick_id = picking_obj.create({
                                'partner_id': partner_id,
                                'origin': picking,
                                'min_date': datetime.now(),
                                'picking_type_id': 1,  # TLI Warehouse: Receipts
                                'location_id': 8,  # Vendor Location
                                'location_dest_id': 15,  # warehouse Stock
                                'state': 'draft',
                                'move_type': 'direct',
                                'priority': '1',
                            })

                prod_sn_list = []
                for data in all_data:
                    if picking == data.get('doc'):
                        # condition for product(code art.) data
                        if data.get('sku_code'):
                            product_id = product_obj.search([('default_code', '=', data.get('internal_ref'))])
                            # if product has value but not found
                            if not product_id:
                                raise Warning('Product (internal reference) is not found = ' + str(data.get('internal_ref')))
                                product_id = None
                            else:
                                desc_product = '[' + str(product_id.default_code) + '] ' + str(product_id.name)
                                product_id = product_id.id
                        else:
                            raise Warning('Column Product is NULL')
                            product_id = None
                            desc_product = ''

                        # condition for qty
                        if data.get('qty') or data.get('qty') == 0.0:
                            move_qty = data.get('qty')
                        else:
                            raise Warning('Column Qty is NULL on xlsx file')

                        lot = self.env['stock.production.lot'].search([('name','=',data.get('sku_desc')),('product_id','=',product_id)])
                        sn_id = None
                        serial_number = None
                        if lot:
                            sn_id = lot.id
                        else:
                            new_lot = self.env['stock.production.lot'].create({
                                'name': data.get('sku_desc'),
                                'product_id': product_id
                            })
                            sn_id = new_lot.id      
                        prod_sn_list.append({
                            'product_id': product_id,
                            'sn':data.get('sku_desc'),
                            'qty':data.get('qty'),
                            'sn_id':sn_id
                        })                  
                        move_obj.create({
                            'product_id': product_id,
                            'product_uom_qty': move_qty,
                            'product_uom': 1,  # default unit(s)
                            'picking_id': pick_id.id,
                            'origin': data.get('doc'),
                            'date_expected': datetime.now(),
                            'state': 'draft',
                            'restrict_lot_id':sn_id,
                            'date': datetime.now(),
                            'picking_type_id': 1,  # TLI Warehouse: Receipts
                            'location_id': 8,  # Vendor Location
                            'location_dest_id': 15,  # warehouse Stock
                            'name': desc_product,
                        })

                # mark as todo the picking and stock move
                pick_id.action_confirm()
                pick_id.action_assign()
                # insert sequence
                # pack operation per stock picking
                pack_ops = pick_id.pack_operation_product_ids

                for pack_op in pack_ops:

                    for prod_sn in prod_sn_list:
                        if pack_op.product_id.id == prod_sn.get('product_id'):

                            #check if sn is in operation lot
                            op_lot = pack_op.pack_lot_ids.search([('lot_id','=',prod_sn.get('sn_id')),('operation_id','=',pack_op.id)])

                            if op_lot:
                                new_qty = op_lot.qty + prod_sn.get('qty')
                                update_op_lot = op_lot.write({
                                    'qty': new_qty,
                                    'lot_name': prod_sn.get('sn')
                                })
                            else:
                                pack_op.pack_lot_ids.create({
                                    'operation_id': pack_op.id,
                                    'lot_id': prod_sn.get('sn_id'),
                                    'qty': prod_sn.get('qty'),
                                    'lot_name': prod_sn.get('sn')
                                })
                    pack_op.save()


        except Exception as error:
            raise UserError(_(str(error) + '\nHave an error, please contact admin aziz@portcities.net'))

        return {'ir.actions.act_window_close'}
