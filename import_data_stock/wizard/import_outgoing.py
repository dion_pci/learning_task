# -*- coding: utf-8 -*-
import base64
from datetime import datetime

import xlrd

from odoo import _, api, fields, models
from odoo.exceptions import UserError, Warning
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT


class ImportOutgoing(models.TransientModel):
    _name = 'import.outgoing'
    _description = 'Import Outgoing Stock'

    file_data = fields.Binary(string='File XLS', required=True)
    file_name = fields.Char(string='File XLS')

    @api.onchange('file_name')
    def change_filename(self):
        if not self.file_name:
            self.file_data = False

    def action_import(self):
        try:
            date_now = datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            book = xlrd.open_workbook(
                file_contents=base64.decodestring(self.file_data))
            sheet = book.sheet_by_index(0)

            # temporary data and picking(per docs)
            all_pickings = []
            all_data = []
            # read and loops xlsx data
            for row in range(sheet.nrows):
                if row == 0:
                    continue
                sku_code = sheet.cell(row, 0).value
                sku_desc = sheet.cell(row, 1).value
                qty = sheet.cell(row, 2).value
                return_qty = sheet.cell(row, 3).value
                customer = sheet.cell(row, 4).value
                id_input = sheet.cell(row, 5).value
                doc = sheet.cell(row, 6).value
                int_ref = sheet.cell(row, 7).value
                type_int_ref = type(int_ref)
                int_ref = int(int_ref) if type_int_ref == float else int_ref

                all_pickings.append(str(doc))
                all_data.append({
                    'sku_code': sku_code if sku_code else None,
                    'sku_desc': sku_desc if sku_desc else None,
                    'qty': qty if qty else 0.0,
                    'return_qty': return_qty,
                    'customer': customer if customer else None,
                    'id_input': int(id_input) if id_input else None,
                    'doc': str(doc),
                    'internal_ref': str(int_ref) if int_ref else None
                })

            picking_obj = self.env['stock.picking']
            product_obj = self.env['product.product']
            partner_obj = self.env['res.partner']
            pickings = [picking for picking in set(all_pickings)]
            for picking in pickings:
                internal_notes = None
                partner_id = None
                for data in all_data:
                    if picking == data.get('doc'):
                        internal_notes = data.get('customer')
                        if internal_notes:
                            partner_id = partner_obj.search([
                                ('comment', '=', internal_notes),
                                ('customer', '=', True)])
                            partner_id = partner_id.id
                        else:
                            partner_id = None

                # create picking use query
                self._cr.execute('''SELECT nextval('stock_picking_id_seq')''')
                pick_id = self._cr.fetchone()
                pick_id = pick_id[0]
                self._cr.execute(
                    '''INSERT INTO stock_picking
                    (id, create_date, write_date, write_uid, create_uid,
                    partner_id, origin, min_date, picking_type_id,
                    location_id, location_dest_id, state, move_type,
                    priority, company_id)
                    VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)''',
                    (pick_id, date_now, date_now, 1, 1,
                        partner_id if partner_id else None, picking,
                        datetime.now(), 4, 15, 9, 'draft', 'direct', 1, 1))

                prod_sn_list = []
                for data in all_data:
                    if picking == data.get('doc'):
                        # condition for product(code art.) data
                        if data.get('sku_code'):
                            product_id = product_obj.search([
                                ('default_code', '=',
                                    data.get('internal_ref'))])
                            # if product has value but not found
                            if not product_id:
                                raise Warning('Product (internal reference) is not found = ' + str(data.get('internal_ref')))
                                product_id = None
                            else:
                                desc_product = '[' + str(product_id.default_code) + '] ' + str(product_id.name)
                                product_id = product_id.id
                        else:
                            product_id = None
                            desc_product = ''
                            raise Warning('Column Product is NULL')

                        # condition for qty
                        if data.get('qty') or data.get('qty') == 0.0:
                            move_qty = data.get('qty')
                        else:
                            raise Warning('Column Qty is NULL on xlsx file')

                        lot = self.env['stock.production.lot'].search([
                            ('name', '=', data.get('sku_desc')),
                            ('product_id', '=', product_id)])
                        sn_id = None
                        if lot:
                            sn_id = lot.id
                        else:
                            self._cr.execute('''SELECT nextval('stock_production_lot_id_seq')''')
                            new_lot = self._cr.fetchone()
                            new_lot = new_lot[0]
                            # product_qty removed due to not exist in database
                            # replaced with product_uom_id
                            self._cr.execute(
                                '''INSERT INTO stock_production_lot
                                (id, create_date, write_date, write_uid,
                                create_uid, name, product_uom_id, product_id)
                                VALUES(%s,%s,%s,%s,%s,%s,%s,%s)''',
                                (new_lot, date_now, date_now, 1, 1,
                                    data.get('sku_desc'), 1, product_id))

                            sn_id = new_lot
                        prod_sn_list.append({
                            'product_id': product_id,
                            'sn': data.get('sku_desc'),
                            'qty': data.get('qty'),
                            'sn_id': sn_id,
                            'picking_id': pick_id,
                            'location': 15,  # Vendor Location
                            'location_dest_id': 9  # warehouse Stock
                        })
                        self._cr.execute('''SELECT nextval('stock_move_id_seq')''')
                        move_id = self._cr.fetchone()
                        move_id = move_id[0]
                        self._cr.execute(
                            '''INSERT INTO stock_move
                            (id, create_date, write_date, write_uid,
                            create_uid, product_id, product_uom_qty,
                            product_uom, picking_id, origin, date_expected,
                            state, restrict_lot_id, date, picking_type_id,
                            location_id, location_dest_id, name,
                            procure_method, company_id, priority, product_qty)
                            VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,
                            %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)''',
                            (move_id, date_now, date_now, 1, 1,
                                product_id, move_qty, 1, pick_id,
                                data.get('doc'), date_now, 'draft', sn_id,
                                date_now, 4, 15, 9, desc_product,
                                'make_to_stock', 1, 1, move_qty))

                pick_ids = picking_obj.browse(pick_id)
                # get picking sequence name and update next number
                sequence_ids = pick_ids.picking_type_id.sequence_id
                sequence = sequence_ids.next_by_id()
                self._cr.execute(
                    '''UPDATE stock_picking
                    SET name = %s
                    WHERE id = %s''',
                    (sequence, pick_id, ))

                # mark as todo the picking and stock move
                pick_ids.action_confirm()
                pick_ids.action_assign()
                # insert sequence
                # pack operation per stock picking
                pack_ops = pick_ids.pack_operation_product_ids

                for pack_op in pack_ops:
                    total_lot_qty = 0.0
                    lot_id = None
                    for prod_sn in prod_sn_list:
                        if pack_op.product_id.id == prod_sn.get('product_id'):
                            # check if sn is in operation lot
                            op_lot = pack_op.pack_lot_ids.search([
                                ('lot_id', '=', prod_sn.get('sn_id')),
                                ('operation_id', '=', pack_op.id)])
                            lot_id = op_lot.id
                            total_lot_qty += prod_sn.get('qty')
                            if op_lot:
                                new_qty = prod_sn.get('qty')
                                self._cr.execute(
                                    '''UPDATE stock_pack_operation_lot
                                    SET qty = %s, lot_name = %s
                                    WHERE id = %s ''',
                                    (new_qty, prod_sn.get('sn'), lot_id))
                                # update op_lot qty
                                temp_qty = op_lot.qty if op_lot.qty else 0
                                temp_qty += new_qty
                                op_lot.write({'qty': temp_qty})
                            else:
                                self._cr.execute('''SELECT nextval('stock_pack_operation_lot_id_seq')''')
                                operation_lot_id = self._cr.fetchone()
                                operation_lot_id = operation_lot_id[0]
                                self._cr.execute(
                                    '''INSERT INTO stock_pack_operation_lot
                                    (id, create_date, write_date, write_uid,
                                    create_uid, lot_id, qty_todo, qty,
                                    operation_id, lot_name)
                                    values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)''',
                                    (operation_lot_id, date_now, date_now,
                                        1, 1, prod_sn.get('sn_id'),
                                        prod_sn.get('qty'), prod_sn.get('qty'),
                                        pack_op.id, prod_sn.get('sn')))
                                lot_id = operation_lot_id

                    pack_op.save()
                    self._cr.execute(
                        '''UPDATE stock_pack_operation
                        SET qty_done = %s
                        WHERE id = %s ''',
                        (pack_op.product_qty, pack_op.id, ))
                    # disable update total qty via sql
                    # self._cr.execute(
                    #     '''UPDATE stock_pack_operation_lot
                    #     SET qty = %s
                    #     WHERE id = %s''',
                    #     (total_lot_qty, lot_id, ))

        except Exception as error:
            raise UserError(_(str(error) + '\nHave an error, please contact admin aziz@portcities.net'))
        return {'ir.actions.act_window_close'}
