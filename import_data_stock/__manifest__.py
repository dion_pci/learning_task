# -*- coding: utf-8 -*-
{
    'name': "Import Stock Data",
    'depends': ['stock'],
    'author': "PT. PCI Business Solution",
    'website': "www.portcities.net",
    'category': "Stock",
    'summary': "Import Stock Data TLI 4Wheels",
    'description': """
V1.0 \n
1. New menu item to import stock data feature on Inventory/Operations/Import Incoming \n
by : Pamungkas \n
2. Create function to import data stock (picking & stock move also) \n
by : Aziz \n
3. Automatically generate serial number/lots when import data \n
by : Heru \n
3. Modify import function for incoming shipment to outgoing shipment\n
by : Dion \n
    """,
    'data': [
        'wizard/import_incoming_views.xml',
        'wizard/import_outgoing_views.xml',
    ],
    'installable': True,
    'application': False,
    'auto-install': False,
}
